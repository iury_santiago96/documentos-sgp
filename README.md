[========]

**Repositórios SGP -> Sistema de Gerênciamento de PET**

[[1] SGP -> BACKEND][BACKEND] 
[BACKEND]: https://bitbucket.org/iury_santiago96/sgp/src/master/ "Repositório para o BACKEND."

----

[[2] SGP -> FRONTEND][FRONTEND]
[FRONTEND]: https://bitbucket.org/iury_santiago96/sgp-frontend-fake/src/master/ "Repositório para o FRONTEND."

----

[[3] SGP -> DOCUMENTOS][DOCUMENTOS]  <--- **Você está nesse repositório.**
[DOCUMENTOS]: https://bitbucket.org/iury_santiago96/documentos-sgp/src/master/ "Repositório para os Documentos"

[========]

**Iniciando o projeto via JAR e nGInx**
----
[1] *abra a pasta SGP. (A mesma se encontra dentro da pasta de documentos*
----
[2] *Instale o **Postgresql** após instalação abra o **pgadmin** basta pesquisar por **pgadmin** no buscador do windows, em seguida crie um banco chamado **sgp** e faça a restauração do banco **SGPDEFAULT.BACKUP**
----
[3] *Entre na pasta SGPBackEnd e clique no bat  **start-sgp.bat** em seguida o bat irá iniciar o servidor do back-end*
----
[4] *Entre na pasta SGPFronEnd após entre na pasta nginx e execute o **nginx.exe** em seguida o nginx irá iniciar o servidor de front-end.*
----
[5] *Para acessar o projeto basta digitar **localhost** em seu navegador que o sistema já estará funcionando 100%*
----
[OBS] -> Qualquer dúvida pode contatar via E-mail: **iury.contato96@gmail.com**
----
[========]